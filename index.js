/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

        

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/
    function getUserInfo() {
        let userInfo = {
            key: "123",
            name: "John Doe",
            age: "25",
            address: "123 Main Street, Quezon City",
            isMarried: "false",
            petName: "Danny",
        };
        
        return userInfo;
    };

    let myUser = getUserInfo();
    console.log("getUserInfo();", myUser);



/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/

    function getArtistsArray() {
        let artist = ["Ben & Ben", "Arthur Nerry", "Linkin Park", "Paramore", "Taylor Swift"];

        return artist;
    };
  
    let artist = getArtistsArray();
    console.log("getArtistArray();", artist);



/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/  

    function getSongsArray() {
        let myFavoriteSongs = ["Kathang Isip", "Binhi", "In the End", "Bring by Boring Brick", "lovestory"];
        
        return myFavoriteSongs;
    };


    let myFavoriteSongs = getSongsArray();
    console.log("getSongsArray();", myFavoriteSongs);






/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

    function getMoviesArray() {
        let myMovies = ["The Lion King", "Meet the Robinsons", "Howl's Moving Castle", "Tangled", "Frozen"];
        
        return myMovies;
    };
  
    const myMovies = getMoviesArray();
    console.log("getMoviesArray();", myMovies);




/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/


    function getPrimeNumberArray() {
        let primes = [];
        let num = 2;
  
        while (primes.length < 5) {
        let isPrime = true;
  
        for (let i = 2; i <= Math.sqrt(num); i++) {
            if (num % i === 0) {
            isPrime = false;
            break;
            };
        };
  
        if (isPrime) {
            primes.push(num);
        };
  
        num++;
        };
  
        return primes;
    };
  
    let primeNumbers = getPrimeNumberArray();
    console.log("getPrimeNumberArray();", primeNumbers);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}